package com.test.blue;

import com.test.aidl.DataBlue2Red;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class BlueService extends Service {
    
    private static final String TAG  =  "BlueService";
    
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
    	Log.i(TAG, "service onBind");
        return mBinder;
    }
    
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
    }
    
    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
    	Log.i(TAG, "service onUnbind");
        return super.onUnbind(intent);
    }
    
    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
    
    private final DataBlue2Red.Stub mBinder = new DataBlue2Red.Stub() {
    	
    	/**
    	 * return X of blue-circle position
    	 */
		@Override
		public float passPosiXFromBlue() throws RemoteException {
			// TODO Auto-generated method stub
			SharedPreferences settings = getSharedPreferences("circledata", 0);
			return settings.getFloat("posiX", 0.0f);
		}
		
		/**
    	 * return Y of blue-circle position
    	 */
		@Override
		public float passPosiYFromBlue() throws RemoteException {
			// TODO Auto-generated method stub
			SharedPreferences settings = getSharedPreferences("circledata", 0);
			return settings.getFloat("posiY", 0.0f);
		}
		
		/**
    	 * return 'text' in blue
    	 */
		@Override
		public String passUserTextFromBlue() throws RemoteException {
			// TODO Auto-generated method stub
			SharedPreferences settings = getSharedPreferences("circledata", 0);
			return settings.getString("text", "Blue");
		}
    };
}