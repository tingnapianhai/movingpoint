package com.test.blue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.test.aidl.DataRed2Blue;

public class BlueActivity extends Activity implements OnTouchListener, OnClickListener{
	
	/**
	 * views 
	 */
	EditText editText;
	TextView text;
	Button btn_confirm;
	LinearLayout layout;
	ImageView blueCircle;
	Button btn_reset;
	TextView posi_x;
	TextView posi_y;
	
	Context con;
	final private String TAG = "BlueActivity";
	
	/**
	 * MY-Blue parameters(x,y,text)
	 */
	final private static String INITTEXT = "Blue";
	float position_x = 0.0f;
	float position_y = 0.0f;
	String userText = INITTEXT;
	
	final private String RedService = "com.test.red.RedService";
	
	/** Flag indicating whether it has called bind on the service. */
    boolean mBound = false;
    
	private DataRed2Blue  mService = null;
	private ServiceConnection mServiceConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			mService = null;
			mBound = false;
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			mService = DataRed2Blue.Stub.asInterface(service);
			mBound = true;
			Log.i(TAG, "service connected");
			//set up the circle-position and text once the connection is done
			setupCircleAndText();
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initializeItems();
		
		/** set up the circle-position and text when the app is opened */
		setupCircleAndText();
	}
	
	private void initializeItems(){
		con = this;
		
		editText = (EditText)findViewById(R.id.et_text);
		text = (TextView)findViewById(R.id.tv_text);
		btn_confirm = (Button)findViewById(R.id.btn_confirm);
		layout = (LinearLayout)findViewById(R.id.linearlayout_up);
		blueCircle = (ImageView)findViewById(R.id.circle);
		btn_reset = (Button)findViewById(R.id.btn_reset);
		posi_x = (TextView)findViewById(R.id.posi_x);
		posi_y = (TextView)findViewById(R.id.posi_y);
		
		btn_confirm.setOnClickListener(this);
		btn_reset.setOnClickListener(this);
		
		layout.setOnTouchListener(this);
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
        Intent intent = new Intent(RedService);
		bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		if(mBound){
			unbindService(mServiceConnection);
			mBound = false;
		}
		
		//save status data: circle-position & text
		saveData();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	/**
	 * save circle-position x,y and text
	 */
	private void saveData(){
		SharedPreferences settings = getSharedPreferences("circledata", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("text", userText);
        editor.putFloat("posiX", position_x);
        editor.putFloat("posiY", position_y);
        editor.commit();
	}

	/**
	 * set up the circle-position and text
	 */
	private void setupCircleAndText(){
		
		/**
		 * if bound, then load data from Red and do the setup
		 */
		if(mBound){
			try {
				position_x = mService.passPosiX();
				position_y = mService.passPosiY();
				userText = mService.passUserText();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.i(TAG, "data from RED");
			Toast.makeText(this, "data received from RED", Toast.LENGTH_SHORT).show();
		}
		
		/**
		 * if not bound (or Red does not exist), then load the previous status of Blue if has, 
		 *                                        otherwise load the default data (0,0,"Red")
		 */
		else {
			SharedPreferences settings = getSharedPreferences("circledata", 0);
			position_x = settings.getFloat("posiX", 0.0f);
			position_y = settings.getFloat("posiY", 0.0f);
			userText = settings.getString("text", "Blue");
		}
		
		blueCircle.setX(position_x);
		blueCircle.setY(position_y);
		text.setText(userText);
		posi_x.setText(" X: " + (int)position_x);
		posi_y.setText("Y: " + (int)position_y);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		position_x = event.getRawX() - blueCircle.getWidth()/2.0f; 
		position_y = event.getRawY() - blueCircle.getHeight()*2.0f;
		switch(event.getAction()){
		/**
		 * blue-circle movement
		 */
		case MotionEvent.ACTION_MOVE:
			blueCircle.setX(position_x);
			blueCircle.setY(position_y);
			posi_x.setText("X: " + (int)position_x);
			posi_y.setText("Y: " + (int)position_y);
			break;
		default: break;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		
		/**
		 * user input text
		 */
		case R.id.btn_confirm:
			userText = editText.getText().toString();
			if(userText==null){
				userText = INITTEXT;
			}
			text.setText(userText);
			Toast.makeText(this, "text updated", Toast.LENGTH_SHORT).show();
			editText.setText("");
			break;
			
		/**
		 * reset circle(x,y) and text
		 */
		case R.id.btn_reset:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Reset circle-position and text ?")
                   .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                	   public void onClick(DialogInterface dialog, int id) {
                		   position_x = 0.0f;
	               		   position_y = 0.0f;
	               		   userText = INITTEXT;
	               		   text.setText(userText);
	               		   blueCircle.setX(position_x);
	               		   blueCircle.setY(position_y);
	               		   posi_x.setText(" X: " + (int)position_x);
	               		   posi_y.setText("Y: " + (int)position_y);
	               		   Toast.makeText(con, "reset done", Toast.LENGTH_SHORT).show();
	                	   }
	                	})
                   .setNegativeButton("Cancel", null);
			AlertDialog dialog = builder.create();
			dialog.show();
			break;
		default: break;
		}
		
	}
	
}
