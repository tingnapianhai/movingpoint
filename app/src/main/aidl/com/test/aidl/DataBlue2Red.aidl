package com.test.aidl;

interface DataBlue2Red {
        float passPosiXFromBlue();
        float passPosiYFromBlue();
        String passUserTextFromBlue();
}