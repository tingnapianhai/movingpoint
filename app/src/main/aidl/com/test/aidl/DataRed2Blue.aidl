package com.test.aidl;

interface DataRed2Blue {
        float passPosiX();
        float passPosiY();
        String passUserText();
}